const USERS = 'users';

function getFromStore(name) {
  const rawUsers = localStorage.getItem(name);
  return JSON.parse(rawUsers);
}

function saveInStore(name, data) {
  localStorage.setItem(name, JSON.stringify(data));
}

function getUsers() {
  return getFromStore(USERS);
}

function addUser(user) {
  const users = getFromStore(USERS) || [];
  users.push(user);
  saveInStore(USERS, users);
}

function updateUser(user) {
  const users = getFromStore(USERS);
  saveInStore(USERS, users.map(item => item.id === user.id ? user : item ));
}

function removeUser(user) {
  const users = getFromStore(USERS);
  saveInStore(USERS, users.filter(item => item.id === user.id));
}

export {
  getUsers,
  addUser,
  updateUser,
  removeUser,
};
