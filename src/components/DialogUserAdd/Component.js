/**
 * Created by vitaliy on 19.06.17.
 */
/**
 *
 * Detailed
 *
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DialogTemplate from '../shared/DialogTemplate';
import './style.scss';
import { Form, Button } from "semantic-ui-react";

const propTypes = {
  open : PropTypes.bool,
  title : PropTypes.string,
  className : PropTypes.string,
  onClose : PropTypes.func,
  onSubmit : PropTypes.func,
  size : PropTypes.string
};
const defaultProps = {
  size: 'small'
};

class DialogDetailedShow extends Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();

    this.state = {
      firstName: null,
      secondName: null
    };
  }

  componentWillReceiveProps(nextProps) {
    const { open } = this.props;
    const { open : newOpen } = nextProps;

    if (open && open !== newOpen) {
      this.setState({
        firstName: null,
        secondName: null
      });
    }
  }

  checkFieldOnError(value) {
    if(Object.is(value, null)) {
      return false; // pristine
    }

    return !value.trim();
  }

  handleChange(field, value) {
    this.setState({[field]: value});
  }

  handleSubmit() {
    const { onSubmit } = this.props;
    const { firstName, secondName } = this.state;

    onSubmit(`${firstName} ${secondName}`);
  }

  render() {
    const { open, title, className, onClose, size } = this.props;
    const { firstName, secondName } = this.state;

    return (
      <DialogTemplate
        open={open}
        className={`dialog-detailed ${className ? className : ''}`}
        size={size}
        title={title}
        onClose={onClose}
      >
        <Form>
          <Form.Field>
            <Form.Input fluid label='First name' placeholder='First name'
                        value={firstName || ''}
                        error={this.checkFieldOnError(firstName)}
                        onChange={(e, v) => this.handleChange('firstName', v.value)}
            />
          </Form.Field>
          <Form.Field>
            <Form.Input fluid label='Last Name' placeholder='Last Name'
                        value={secondName || ''}
                        error={this.checkFieldOnError(secondName)}
                        onChange={(e, v) => this.handleChange('secondName', v.value)}
            />
          </Form.Field>
          <Button type='submit'
                  disabled={!(firstName || '').trim() || !(secondName || '').trim()}
                  onClick={::this.handleSubmit}
          >Save user</Button>
        </Form>
      </DialogTemplate>
    );
  }
}

DialogDetailedShow.propTypes = propTypes;
DialogDetailedShow.defaultProps = defaultProps;

export default DialogDetailedShow;
