/*
* Users Selectors
*
* This contains all the text for the Users component.
*/

import { createStructuredSelector } from 'reselect';

export const modelSelector = createStructuredSelector({
  users: state => state.users.users || []
});
