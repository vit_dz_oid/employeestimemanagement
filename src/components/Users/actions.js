/*
* Users Actions
*
* This contains all the text for the Users component.
*/
import { bindActionCreators } from 'redux';

export  const USERS_GET = 'USERS/USERS_GET';
export  const USERS_SET = 'USERS/USERS_SET';
export  const USERS_ADD = 'USERS/USERS_ADD';
export  const USERS_REMOVE = 'USERS/USERS_REMOVE';
export  const USERS_RESTORE = 'USERS/USERS_RESTORE';
export  const USERS_SUCCESS = 'USERS/USERS_SUCCESS';
export  const USERS_ERROR = 'USERS/USERS_ERROR';

function getUsers() {
  return { type: USERS_GET };
}

function setUsers(users) {
  return { type: USERS_SET, payload: { data: users } };
}

function addUser(name) {
  return { type: USERS_ADD, payload: { data: name } };
}

function removeUser(id) {
  return { type: USERS_REMOVE, payload: { data: id } };
}

function restoreUser(id) {
  return { type: USERS_RESTORE, payload: { data: id } };
}

function containerActions(dispatch) {
  return bindActionCreators({ getUsers, setUsers, addUser, removeUser, restoreUser }, dispatch);
}

export { containerActions, getUsers, setUsers, addUser, removeUser, restoreUser };
