import React from 'react';
import { Button, Icon, Item } from "semantic-ui-react";

export default ({ name, dateStart, dateEnd, onAction }) => (<Item className="user">
  <Item.Image size='tiny' src='https://react.semantic-ui.com/assets/images/avatar/large/justen.jpg' />

  <Item.Content verticalAlign='middle'>
    <Item.Header>
      <Icon name='like' />
      {name}
    </Item.Header>
    <Item.Meta>
      <p>User exist in team:</p>
      <span>{dateStart}</span> - <span>{dateEnd ? dateEnd : 'still working'}</span>
    </Item.Meta>
    <Item.Extra>
      { dateEnd ?
        <Button primary floated='right' icon='repeat' content='Restore' onClick={() => onAction(false)}/> :
        <Button color='red' floated='right' icon='remove user' content='Remove user' onClick={() => onAction(true)}/> }
    </Item.Extra>
  </Item.Content>

</Item>);
