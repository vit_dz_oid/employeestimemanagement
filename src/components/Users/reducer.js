/*
* Users Reducer
*
* This contains all the text for the Users component.
*/
import * as actions from './actions.js';

const initialState = {
  users: []
};

export default function (state = initialState, action = {}) { // , action = {}
  const { type, payload } = action;

  if(type === actions.USERS_SET) {
    return { ...state, users: payload.data };
  }
  /*
  // TEMPLATE
  if(type === actions) {
    return { ...state, };
  }
  */
  return state;
}
