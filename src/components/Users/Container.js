/**
*
* Users
*
*/

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Icon, Item } from 'semantic-ui-react'
import { modelSelector } from './selectors.js';
import { containerActions } from './actions.js';
import User from "./User";
import DialogAddUser from '../DialogUserAdd';

import './Users.scss';

const propTypes = {
  users: PropTypes.array,
};
const defaultProps = {
  users: [
    {
      name: 'Grigoriy Chudnov',
      dateStart: '21/02/15',
      dateEnd: null
    },
    {
      name: 'Vasya Pupkin',
      dateStart: '22/03/15',
      dateEnd: '30/12/17'
    },
    {
      name: 'Albatros Fits',
      dateStart: '23/04/15',
      dateEnd: '05/05/16'
    },
    {
      name: 'Yuhal Ordzhonikidze',
      dateStart: '25/05/15',
      dateEnd: null
    },
    {
      name: 'Ahuvi Israel',
      dateStart: '05/09/14',
      dateEnd: '25/05/15'
    },
    {
      name: 'Leha Driver',
      dateStart: '17/10/16',
      dateEnd: null
    },
    {
      name: 'Franklin Usaovich',
      dateStart: '22/08/11',
      dateEnd: '01/01/18'
    },
    {
      name: 'Petro Incognito',
      dateStart: '13/07/17',
      dateEnd: null
    },
    {
      name: 'Zhora Poc',
      dateStart: '30/07/16',
      dateEnd: '11/02/18'
    },
    {
      name: 'Vitos Barbos',
      dateStart: '07/06/13',
      dateEnd: null
    },
    {
      name: 'Andrew Krendel',
      dateStart: '31/03/15',
      dateEnd: '25/12/17'
    },
    {
      name: 'Yuliya Vertuhay',
      dateStart: '21/02/14',
      dateEnd: '12/12/17'
    },
  ]
};

class Users extends Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { getUsers } = this.props;

    getUsers();
  }

  constructor() {
    super();

    this.state = {
      openModal: false
    };
  }

  toggleModal() {
    this.setState({
      openModal: !this.state.openModal
    });
  }

  handleAddUser(name) {
    const { addUser } = this.props;

    addUser(name);
    this.toggleModal();
  }

  toggleUserState(state, id) {
    const { removeUser, restoreUser } = this.props;

    state ? removeUser(id) : restoreUser(id);
  }

  render() {
    const { users } = this.props;
    const { openModal } = this.state;

    return [
      <Item.Group key="0" className="users-component">
        {users.map(user => <User key={user.name} {...user} onAction={(newState) => this.toggleUserState(newState, user.id)}/>)}
      </Item.Group>,
      <Button key="1" primary icon='add user' content='Add user' onClick={::this.toggleModal}/>,
      <DialogAddUser
        key="2"
        open={openModal}
        onClose={::this.toggleModal}
        onSubmit={::this.handleAddUser}
        title="Add new user"
        size="tiny"
      />
    ];
  }
}

Users.propTypes = propTypes;
Users.defaultProps = defaultProps;

export default connect(modelSelector, containerActions)(Users);
