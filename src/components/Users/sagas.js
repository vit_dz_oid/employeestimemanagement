/*
* Users Actions
*
* This contains all the text for the Users component.
*/
import { takeEvery, call, put } from 'redux-saga/effects'; // , take, call, put, race
import * as serverApi from 'api/serverAPI';
import * as actions from './actions.js';
import { randomDate } from 'helpers/utils';
import { COMMON_USER_DATE_FORMAT } from 'config/config'
import moment from 'moment';

function* getUsers() {
  const users = yield call(serverApi.getUsers);
  yield put(actions.setUsers(users));
}

function* addUser(action) {
  const current = (new Date()).getTime();
  const user = {
    id: current,
    name: action.payload.data,
    dateStart: moment(randomDate(new Date(2010, 0, 1), new Date())).format(COMMON_USER_DATE_FORMAT),
    dateEnd: null,
  };

  yield call(serverApi.addUser, user);
  yield getUsers();
}

function* removeUser(action) {
  const users = yield call(serverApi.getUsers);
  const user = users.find(item => item.id === action.payload.data);
  user.dateEnd = moment(randomDate(moment(user.dateStart, COMMON_USER_DATE_FORMAT).toDate(), new Date()))
    .format(COMMON_USER_DATE_FORMAT);
  yield call(serverApi.updateUser, user);
  yield getUsers();
}

function* restoreUser(action) {
  const users =  yield call(serverApi.getUsers);
  const user = users.find(item => item.id === action.payload.data);
  user.dateEnd = null;
  yield call(serverApi.updateUser, user);
  yield getUsers();
}

export default function* mainSaga() {
  yield takeEvery(actions.USERS_GET, getUsers);
  yield takeEvery(actions.USERS_ADD, addUser);
  yield takeEvery(actions.USERS_REMOVE, removeUser);
  yield takeEvery(actions.USERS_RESTORE, restoreUser);
}
