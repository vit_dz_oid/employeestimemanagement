import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, Icon } from 'semantic-ui-react';

const propTypes = {
  title : PropTypes.string,
  actions : PropTypes.any,
  open : PropTypes.bool,
  children : PropTypes.object,
  onClose : PropTypes.func,
  className : PropTypes.string,
  size : PropTypes.string
};

const defaultProps = {};

class DialogTemplate extends Component {

  sizeClass(size) {
    switch (size) {
      case 'small' :
        return 'dialog-small';
      default:
        return '';
    }
  }

  render() {
    const { title, actions, open, className, onClose, size } = this.props;
    const [titleP1, ...titleP2] = title.split(' ');

    return (
      <Modal open={open}
             className={`dialog ${className ? className : ''}`}
             size={size}
             onClose={onClose}>
        <Modal.Content>
          <div className='dialog__close'>
            <Icon name='remove' size='large' onClick={onClose}/>
          </div>
          <h3 className='dialog__title'><span>{titleP1}</span> {titleP2.join(' ')}</h3>
          <div className='dialog__content'>
            {this.props.children}
          </div>
          <div className='dialog__buttons'>
            {actions}
          </div>
        </Modal.Content>
      </Modal>
    );
  }
}

DialogTemplate.propTypes = propTypes;
DialogTemplate.defaultProps = defaultProps;

export default DialogTemplate;
