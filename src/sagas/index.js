import { fork } from 'redux-saga/effects';
import { saga as usersSaga } from '../components/Users';

export default function* mainSaga() {
  yield fork(usersSaga);
}
