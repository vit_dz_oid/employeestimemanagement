/**
 * Created by DzEN on 29.01.2017.
 */
import { combineReducers } from 'redux';
import { reducer as usersReducer } from 'components/Users/index';

export default combineReducers({
  users: usersReducer,
});
