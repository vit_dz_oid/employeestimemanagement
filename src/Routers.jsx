import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './pages/Home';
import Statistic from './pages/Statistic';
import Administration from './pages/Administration';

export default () => <Switch>
  <Route exact path='/' component={Home}/>
  <Route path='/statistic' component={Statistic}/>
  <Route path='/administration' component={Administration}/>
  <Route />
</Switch>
