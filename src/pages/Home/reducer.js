/*
* Home Reducer
*
* This contains all the text for the Home component.
*/
import * as actions from './actions.js';

const initialState = {
  users : []
};

export default function (state = initialState, action = {}) {
  const { type, payload } = action;

  if (type === actions.HOME_SET) {
    return { ...state, users: payload.users };
  }

  return state;
}
