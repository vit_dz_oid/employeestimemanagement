/*
* Home Actions
*
* This contains all the text for the Home component.
*/
import { takeEvery, put, call, select } from 'redux-saga/effects'; // , take, call, put, race
import * as actions from './actions.js';

function* getHome(action) {
  const { payload } = action;

  try {
    const res = yield call();

    yield put(actions.setHome(res.data));
  } catch (e) {
    console.error('error ', e);
  }
}

export default function* mainSaga() {
  yield takeEvery('*')
}
