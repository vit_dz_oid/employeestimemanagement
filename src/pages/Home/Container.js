/**
*
* Home
*
*/

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Container, Header } from "semantic-ui-react";
import Users from 'components/Users';

const propTypes = {
  home : PropTypes.array,
};
const defaultProps = {
  home: []
};

class Home extends Component { // eslint-disable-line react/prefer-stateless-function
  render() {

    return  (
      <Container className='home-container'>
        <Header as='h2'>Users</Header>
        <Users />
      </Container>
    );
  }
}

Home.propTypes = propTypes;
Home.defaultProps = defaultProps;

export default Home;
