/*
* Home Actions
*
* This contains all the text for the Home component.
*/
import { bindActionCreators } from 'redux';
export const HOME_GET = 'Home/HOME_GET';
export const HOME_SET = 'Home/HOME_SET';
export const HOME_CALL_SHOW_ADD_DIALOG = 'Home/HOME_CALL_SHOW_ADD_DIALOG';
export const HOME_CALL_SHOW_EDIT_DIALOG = 'Home/HOME_CALL_SHOW_EDIT_DIALOG';
export const HOME_CALL_SHOW_REMOVE_DIALOG = 'Home/HOME_CALL_SHOW_REMOVE_DIALOG';

function getHome(farmId) {
  return { type : HOME_GET, payload : { farmId } };
}

function setHome(home) {
  return { type :HOME_SET, payload : { home } };
}

function showHomeAddDialog() {
  return { type : HOME_CALL_SHOW_ADD_DIALOG };
}

function showHomeEditDialog(home) {
  return { type : HOME_CALL_SHOW_EDIT_DIALOG, payload : { home } };
}

function showHomeRemoveDialog(home) {
  return { type : HOME_CALL_SHOW_REMOVE_DIALOG, payload : { home } };
}

function containerActions(dispatch) {
  return bindActionCreators({ getHome, setHome, showHomeAddDialog,
    showHomeEditDialog, showHomeRemoveDialog }, dispatch);
}

export { containerActions, getHome, setHome, showHomeAddDialog,
    showHomeEditDialog, showHomeRemoveDialog };
