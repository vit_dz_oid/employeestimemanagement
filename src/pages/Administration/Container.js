/**
*
* Admininstration
*
*/

import React, { Component } from 'react';
import PropTypes from 'prop-types';
/*import { connect } from 'react-redux';
import { modelSelector } from './selectors.js';
import { containerActions } from './actions.js';*/

const propTypes = {
  administration : PropTypes.array,
};
const defaultProps = { };

class Administration extends Component { // eslint-disable-line react/prefer-stateless-function

  render() {
    return (
      <div className='administration-container'>
        <div className='page-title'>
          <h1>
            Administration page
          </h1>
        </div>
      </div>
    );
  }
}

Administration.propTypes = propTypes;
Administration.defaultProps = defaultProps;

export default Administration;
