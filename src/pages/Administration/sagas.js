/*
* Admininstration Actions
*
* This contains all the text for the Admininstration component.
*/
import { takeEvery, put, call, select } from 'redux-saga/effects'; // , take, call, put, race
import * as actions from './actions.js';
import { actions as dialogAddActions } from 'client/components/dialogs/admininstration/DialogAdmininstrationAdd';
import { actions as dialogEditActions } from 'client/components/dialogs/admininstration/DialogAdmininstrationEdit';
import { actions as dialogRemoveActions } from 'client/components/dialogs/admininstration/DialogAdmininstrationRemove';
import { getFarmId } from 'helpers/selectorsHelper';
import serverApi from 'api';

function* getAdmininstration(action) {
  const { payload } = action;

  try {
    const res = yield call(serverApi.getAdmininstration, payload.farmId);

    yield put(actions.setAdmininstration(res.data));
  } catch (e) {
    console.error('error ', e);
  }
}

function* addAdmininstrationDialogShow() {
  yield put(dialogAddActions.openDialog());
}

function* editAdmininstrationDialogShow(action) {
  yield put(dialogEditActions.openDialog(action.payload.admininstration));
}

function* removeAdmininstrationDialogShow(action) {
  yield put(dialogRemoveActions.openDialog(action.payload.admininstration));
}

function* updateAdmininstration() {
  const state = yield select();
  const data = {
    payload : {
      farmId : getFarmId(state)
    }
  };

  yield getAdmininstration(data);
}

export default function* mainSaga() {
  yield takeEvery(actions.ADMININSTRATION_GET, getAdmininstration);
  yield takeEvery(actions.ADMININSTRATION_CALL_SHOW_ADD_DIALOG, addAdmininstrationDialogShow);
  yield takeEvery(actions.ADMININSTRATION_CALL_SHOW_EDIT_DIALOG, editAdmininstrationDialogShow);
  yield takeEvery(actions.ADMININSTRATION_CALL_SHOW_REMOVE_DIALOG, removeAdmininstrationDialogShow);
  yield takeEvery(dialogAddActions.DIALOG_ADMININSTRATION_SUBMIT_SUCCESS, updateAdmininstration);
  yield takeEvery(dialogEditActions.DIALOG_ADMININSTRATION_SUBMIT_SUCCESS, updateAdmininstration);
  yield takeEvery(dialogRemoveActions.DIALOG_ADMININSTRATION_SUBMIT_SUCCESS, updateAdmininstration);
}
