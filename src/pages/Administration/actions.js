/*
* Administration Actions
*
* This contains all the text for the Administration component.
*/
import { bindActionCreators } from 'redux';
export const ADMINISTRATION_GET = 'Administration/ADMINISTRATION_GET';
export const ADMINISTRATION_SET = 'Administration/ADMINISTRATION_SET';
export const ADMINISTRATION_CALL_SHOW_ADD_DIALOG = 'Administration/ADMINISTRATION_CALL_SHOW_ADD_DIALOG';
export const ADMINISTRATION_CALL_SHOW_EDIT_DIALOG = 'Administration/ADMINISTRATION_CALL_SHOW_EDIT_DIALOG';
export const ADMINISTRATION_CALL_SHOW_REMOVE_DIALOG = 'Administration/ADMINISTRATION_CALL_SHOW_REMOVE_DIALOG';

function getAdministration(farmId) {
  return { type : ADMINISTRATION_GET, payload : { farmId } };
}

function setAdministration(administration) {
  return { type :ADMINISTRATION_SET, payload : { administration } };
}

function showAdministrationAddDialog() {
  return { type : ADMINISTRATION_CALL_SHOW_ADD_DIALOG };
}

function showAdministrationEditDialog(administration) {
  return { type : ADMINISTRATION_CALL_SHOW_EDIT_DIALOG, payload : { administration } };
}

function showAdministrationRemoveDialog(administration) {
  return { type : ADMINISTRATION_CALL_SHOW_REMOVE_DIALOG, payload : { administration } };
}

function containerActions(dispatch) {
  return bindActionCreators({ getAdministration, setAdministration, showAdministrationAddDialog,
    showAdministrationEditDialog, showAdministrationRemoveDialog }, dispatch);
}

export { containerActions, getAdministration, setAdministration, showAdministrationAddDialog,
    showAdministrationEditDialog, showAdministrationRemoveDialog };
