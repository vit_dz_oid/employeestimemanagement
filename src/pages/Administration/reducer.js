/*
* Admininstration Reducer
*
* This contains all the text for the Admininstration component.
*/
import * as actions from './actions.js';

const initialState = {
  admininstration : []
};

export default function (state = initialState, action = {}) {
  const { type, payload } = action;

  if (type === actions.ADMININSTRATION_SET) {
    return { ...state, admininstration : payload.admininstration };
  }

  return state;
}
