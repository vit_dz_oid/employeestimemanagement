/*
* Admininstration Selectors
*
* This contains all the text for the Admininstration component.
*/

import { createStructuredSelector } from 'reselect';

export const modelSelector = createStructuredSelector({
  administration: state => state.administrationPage.admininstration,
});
