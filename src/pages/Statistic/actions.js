/*
* Statistic Actions
*
* This contains all the text for the Statistic component.
*/
import { bindActionCreators } from 'redux';
export const STATISTIC_GET = 'Statistic/STATISTIC_GET';
export const STATISTIC_SET = 'Statistic/STATISTIC_SET';
export const STATISTIC_CALL_SHOW_ADD_DIALOG = 'Statistic/STATISTIC_CALL_SHOW_ADD_DIALOG';
export const STATISTIC_CALL_SHOW_EDIT_DIALOG = 'Statistic/STATISTIC_CALL_SHOW_EDIT_DIALOG';
export const STATISTIC_CALL_SHOW_REMOVE_DIALOG = 'Statistic/STATISTIC_CALL_SHOW_REMOVE_DIALOG';

function getStatistic(farmId) {
  return { type : STATISTIC_GET, payload : { farmId } };
}

function setStatistic(statistic) {
  return { type :STATISTIC_SET, payload : { statistic } };
}

function showStatisticAddDialog() {
  return { type : STATISTIC_CALL_SHOW_ADD_DIALOG };
}

function showStatisticEditDialog(statistic) {
  return { type : STATISTIC_CALL_SHOW_EDIT_DIALOG, payload : { statistic } };
}

function showStatisticRemoveDialog(statistic) {
  return { type : STATISTIC_CALL_SHOW_REMOVE_DIALOG, payload : { statistic } };
}

function containerActions(dispatch) {
  return bindActionCreators({ getStatistic, setStatistic, showStatisticAddDialog,
    showStatisticEditDialog, showStatisticRemoveDialog }, dispatch);
}

export { containerActions, getStatistic, setStatistic, showStatisticAddDialog,
    showStatisticEditDialog, showStatisticRemoveDialog };
