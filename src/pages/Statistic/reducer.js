/*
* Statistic Reducer
*
* This contains all the text for the Statistic component.
*/
import * as actions from './actions.js';

const initialState = {
  statistic : []
};

export default function (state = initialState, action = {}) {
  const { type, payload } = action;

  if (type === actions.STATISTIC_SET) {
    return { ...state, statistic : payload.statistic };
  }

  return state;
}
