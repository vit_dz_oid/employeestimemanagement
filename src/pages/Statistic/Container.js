/**
*
* Statistic
*
*/

import React, { Component } from 'react';
import PropTypes from 'prop-types';
/*import { connect } from 'react-redux';
import { modelSelector } from './selectors.js';
import { containerActions } from './actions.js';*/

const propTypes = {
  statistic : PropTypes.array,
};
const defaultProps = { };

class Statistic extends Component { // eslint-disable-line react/prefer-stateless-function
  render() {

    return (
      <div className='statistic-container'>
        <div className='page-title'>
          <h1>
            Statistic page
          </h1>
        </div>
      </div>
    );
  }
}

Statistic.propTypes = propTypes;
Statistic.defaultProps = defaultProps;

export default Statistic;
