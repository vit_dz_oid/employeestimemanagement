/*
* Statistic Selectors
*
* This contains all the text for the Statistic component.
*/

import { createStructuredSelector } from 'reselect';

export const modelSelector = createStructuredSelector({
  statistic: state => state.statisticPage.statistic,
});
