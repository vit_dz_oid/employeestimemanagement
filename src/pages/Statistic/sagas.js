/*
* Statistic Actions
*
* This contains all the text for the Statistic component.
*/
import { takeEvery, put, call, select } from 'redux-saga/effects'; // , take, call, put, race
import * as actions from './actions.js';
import { actions as dialogAddActions } from 'client/components/dialogs/statistic/DialogStatisticAdd';
import { actions as dialogEditActions } from 'client/components/dialogs/statistic/DialogStatisticEdit';
import { actions as dialogRemoveActions } from 'client/components/dialogs/statistic/DialogStatisticRemove';
import { getFarmId } from 'helpers/selectorsHelper';
import serverApi from 'api';

function* getStatistic(action) {
  const { payload } = action;

  try {
    const res = yield call(serverApi.getStatistic, payload.farmId);

    yield put(actions.setStatistic(res.data));
  } catch (e) {
    console.error('error ', e);
  }
}

function* addStatisticDialogShow() {
  yield put(dialogAddActions.openDialog());
}

function* editStatisticDialogShow(action) {
  yield put(dialogEditActions.openDialog(action.payload.statistic));
}

function* removeStatisticDialogShow(action) {
  yield put(dialogRemoveActions.openDialog(action.payload.statistic));
}

function* updateStatistic() {
  const state = yield select();
  const data = {
    payload : {
      farmId : getFarmId(state)
    }
  };

  yield getStatistic(data);
}

export default function* mainSaga() {
  yield takeEvery(actions.STATISTIC_GET, getStatistic);
  yield takeEvery(actions.STATISTIC_CALL_SHOW_ADD_DIALOG, addStatisticDialogShow);
  yield takeEvery(actions.STATISTIC_CALL_SHOW_EDIT_DIALOG, editStatisticDialogShow);
  yield takeEvery(actions.STATISTIC_CALL_SHOW_REMOVE_DIALOG, removeStatisticDialogShow);
  yield takeEvery(dialogAddActions.DIALOG_STATISTIC_SUBMIT_SUCCESS, updateStatistic);
  yield takeEvery(dialogEditActions.DIALOG_STATISTIC_SUBMIT_SUCCESS, updateStatistic);
  yield takeEvery(dialogRemoveActions.DIALOG_STATISTIC_SUBMIT_SUCCESS, updateStatistic);
}
