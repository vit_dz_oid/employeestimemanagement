import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import getStore from './store/store';

import Routers from './Routers';

export default class App extends Component {
  render() {
    return (
      <Provider store={getStore()}>
        <BrowserRouter>
          <Routers />
        </BrowserRouter>
      </Provider>
    );
  }
}